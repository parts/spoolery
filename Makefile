-include Makefile.local

OPENSCAD?=time openscad

default:
	@echo "And?"

clean:
	rm -f *.stl *.gcode

%.stl: %.scad
	$(OPENSCAD) $(OPENSCAD_FLAGS) -o "$@" "$<"
