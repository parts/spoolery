use <spool-roller.scad>;

spool_roller(
 b_od = 10, b_id = 3, b_h=4,
 b_iod = 5,
 b_aclearance = 2
);
