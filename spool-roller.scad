/* Main */
// (in mm)
bearing_outer_d = 13;
// (in mm)
bearing_inner_d = 4;
// (in mm)
bearing_width = 5;

// The distance between two bearings (in mm)
bearings_span = 80;

// (in mm)
screw_length=10;


/* [Hidden] */
layer_height=.2; extrusion_width=.5;
epsilon=.01;

module spool_roller(
 b_od=bearing_outer_d,					// bearing outer diameter
 b_id=bearing_inner_d,					// bearings inner diameter
 b_h=bearing_width,					// bearing dimensions
 b_iod=bearing_inner_d+2,				// bearing inner ring outer diameter
 b_span=bearings_span,					// distance between bearings
 b_rclearance=2,					// bearing radial clearance
 b_aclearance=1,					// bearing axial clearance
 screw_l=screw_length,					// screw length
 screw_d=3,						// screw diameter
 screw_hd=6,						// screw head diameter
 screw_hh=3,						// screw head height
 nut_d = 5.45/cos(30),					// nut diameter
 nut_h = 2.5						// nut height
) {
 it = b_h+b_aclearance;     // inner thickness
 ot = screw_l+screw_hh;     // outer thickness
 s = (ot-it)/2;             // shell thickness
 osr = b_od/2+b_rclearance; // outer shell radius
 bs = s;                    // bottom shell
 cuto = osr/4;
 cutr = (cuto*cuto+b_span*b_span/4-osr*osr)/(osr+cuto)/2;
 difference() {
  union() {
   for(sx=[-1,1])
    translate([sx*b_span/2,0,bs+osr]) rotate([90,0,0])
    cylinder(r=osr,h=ot,center=true,$fn=osr*PI*4);
   translate([-b_span/2-osr,-ot/2,0])
   cube(size=[b_span+2*osr,ot,bs+osr+epsilon]);
   rotate([90,0,0])
   linear_extrude(height=ot,center=true)
   translate([0,bs]) polygon([
    [-b_span/2, osr],
    [ 0, cutr+osr-cuto],
    [ b_span/2, osr]
   ]);
   lh = layer_height*2;
   hull() {
    ro=3;
    linear_extrude(height=lh)
    offset(r=ro,$fn=4*PI*ro) offset(r=-ro,$fn=4*PI*ro)
    square(size=[b_span+2*osr+2*bs-2*lh,ot+2*bs+2*lh],center=true);
    translate([0,0,bs/2+epsilon/2])
    cube(size=[b_span+2*osr,ot,bs+epsilon],center=true);
   }
  }//union
  // big cutout
  translate([0,0,bs+cutr+osr-cuto]) rotate([90,0,0])
  cylinder(r=cutr,h=ot+2,center=true,$fn=cutr*PI*4);
  // spool cutout
  difference() {
   translate([-b_span/2-osr-1,-it/2,bs])
   cube(size=[b_span+2*osr+2,it,2*osr+1]);
   for(my=[0,1]) mirror([0,my,0])
   for(sx=[-1,1])
   translate([sx*b_span/2,it/2-b_aclearance/2,bs+osr])
   rotate([-90,0,0])
   cylinder(d1=b_iod,d2=b_iod+2*b_aclearance,h=b_aclearance,$fn=(b_iod+b_aclearance)*PI*2);
  }
  // screwholes
  for(mx=[0,1]) mirror([mx,0,0])
  translate([b_span/2,0,bs+osr]) {
   translate([0,-ot/2+screw_hh,0]) rotate([90,0,0])
   cylinder(d=screw_hd,h=screw_hh+1,$fn=screw_hd*PI*2);
   translate([0,ot/2-nut_h,0]) rotate([-90,0,0])
   rotate([0,0,30]) cylinder(d=nut_d,h=nut_h+1,$fn=6);
   rotate([90,0,0])
   cylinder(d=screw_d,h=ot+2,center=true,$fn=screw_d*PI*2);
  }
 }//difference
 // bearings
 % for(sx=[-1,1]) {
  translate([sx*b_span/2,0,bs+osr]) rotate([90,0,0])
  difference() {
   cylinder(d=b_od,h=b_h,center=true,$fn=b_od*PI*2);
   cylinder(d=b_id,h=b_h+2,center=true,$fn=b_id*PI*2);
  }
 }
}

spool_roller();
